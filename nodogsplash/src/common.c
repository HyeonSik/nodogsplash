#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip_icmp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <string.h>

int getStringCount(FILE *file) {
  if (file == NULL) {
    return -1;
  }

  if (fseek(file, 0, SEEK_END) != 0) {
    return -1;
  }

  int stringCount = ftell(file);
  rewind(file);
  return stringCount;
}

int is_number(char *s) {
  while (*s != '\0') {
    if (!isdigit(*s++))
      return 0;
  }

  return 1;
}

unsigned short static ping_checksum(unsigned short *buf, int buf_size)
{
  unsigned long sum = 0;

  while (buf_size > 1) {
    sum += *buf;
    buf++;
    buf_size -= 2;
  }

  if (buf_size == 1) {
    sum += *(unsigned char *)buf;
  }

  sum = (sum & 0xffff) + (sum >> 16);
  sum = (sum & 0xffff) + (sum >> 16);

  return ~sum;
}

int ping(char *host) {

  if (host == NULL) {
    printf("호스트의 값이 NULL입니다.\n");
    return 1;
  }

  if (strlen(host) <= 0) {
    printf("호스트의 값이 올바르지 않습니다.\n");
    return 1;
  }

  struct sockaddr_in socket_address;
  socket_address.sin_family = AF_INET;
  socket_address.sin_addr.s_addr = inet_addr(host);

  int ping_socket;
  ping_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

  if (ping_socket < 0) {
    printf("common: ping: 소켓 초기화에 실패하였습니다.\n");
    return 1;
  }

  struct timeval timeout;
  timeout.tv_sec = 1;
  timeout.tv_usec = 0;

  if (setsockopt(ping_socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
    printf("common: ping: 받는 소켓 타임아웃 설정 실패\n");
    close(ping_socket);
    return 1;
  }

  if (setsockopt(ping_socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
    printf("common: ping: 보내는 소켓 타임아웃 설정 실패\n");
    close(ping_socket);
    return 1;
  }

  struct icmphdr icmp_header;
  memset(&icmp_header, 0, sizeof(icmp_header));
  icmp_header.type = ICMP_ECHO;
  icmp_header.code = 0;
  icmp_header.checksum = 0;
  icmp_header.un.echo.id = 0;
  icmp_header.un.echo.sequence = 0;
  icmp_header.checksum = ping_checksum((unsigned short *)&icmp_header, sizeof(icmp_header));

  if (sendto(ping_socket, (char *)&icmp_header, sizeof(icmp_header), 0, (struct sockaddr *)&socket_address, sizeof(socket_address)) < 1) {
    printf("common: ping: 클라이언트에 핑을 보내는데 실패하였습니다.\n");

    if (close(ping_socket) < 0) {
      printf("common: ping: 클라이언트 소켓을 닫는데 실패하였습니다.\n");
    }

    return 1;
  }

  char* readbuf = (char*)malloc(sizeof(char) * 1024);
  memset(readbuf, 0, 1024);

  if (recv(ping_socket, readbuf, sizeof(readbuf), 0) < 1) {
    printf("common: ping: 클라이언트에 핑 답변을 받는데 실패하였습니다.\n");

    if (close(ping_socket) < 0) {
      printf("common: ping: 클라이언트 소켓을 닫는데 실패하였습니다.\n");
    }

    free(readbuf);

    return 1;
  }

  struct iphdr *ip_header_ptr;
  struct icmphdr *icmp_header_ptr;

  ip_header_ptr = (struct iphdr *)readbuf;
  icmp_header_ptr = (struct icmphdr *)(readbuf + (ip_header_ptr->ihl * 4));

  if (close(ping_socket) < 0) {
    printf("common: ping: 클라이언트 소켓을 닫는데 실패하였습니다.\n");
  }

  free(readbuf);

  if (icmp_header_ptr->type == ICMP_ECHOREPLY) {
    return 0;
  } else {
    return 1;
  }
}

#define TRY_TIMES 5

int is_alive(char *host) {
  int rc = 1;

  for(int i = 0; i < TRY_TIMES; i++) {
    if (ping(host) == 0) {
      rc = 0;
      break;
    }
  }

  return (!rc);
}
